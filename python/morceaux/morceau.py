from typing import Any, Callable, Iterable, Optional

import itertools
import numpy as np

import morceaux.rust_backend as rs


def __correct_indices__(idxs: Iterable, shifts: np.ndarray) -> tuple:
    return tuple((shifts <= i).sum(dtype='uint') + i for i in idxs)


def morceau(
        records: Iterable[Any],
        batch_size: int,
        return_indices=False,
        size_calc_fn: Optional[Callable[[Any], int]] = None,
        chunk_size: Optional[int] = 2**10
) -> None:
    """Return an iterator in batches over the input data. The order of the element
    minimise the maximum (*sort of*) weight of the batches.

    Args:
        records (Iterable[Any]): Any data source you want to iterate on
        batch_size (int): The size of the batches. Last batch will maybe not be of length `batch_size`
        return_indices (bool, optional): Whether to return the index of every yielded element inside the original data source. Defaults to False.
        size_calc_fn (Optional[Callable[[Any], int]], optional): A function used to compute the weight of every element. If None, identity will be used. Defaults to None.
        chunk_size Optional[int]: Optimisation is made on chunks of data to guarantee short computation time. Defaults to 2**10.

    Returns:
        StopIteration: signals the end of the iterator

    Yields:
        Union[(tuple, list), list]: _description_
    """
    assert batch_size < chunk_size
    records = iter(records)
    chunk = list(itertools.islice(records, chunk_size))
    size_calc_fn = (lambda x: x) if size_calc_fn is None else size_calc_fn

    while len(chunk) > 0:
        weights = np.array([size_calc_fn(e) for e in chunk], dtype='uint')
        last_batch_size = len(weights) % batch_size
        last_batch = None
        if last_batch_size:
            m = weights.mean() * batch_size / last_batch_size
            last_indices = np.argsort((weights - m)**2)[:last_batch_size]
            last_batch = [chunk[i] for i in last_indices]
            chunk = [c for i, c in enumerate(
                chunk) if i not in last_indices]  # find better
            weights = np.delete(weights, last_indices, 0)
            shifts = np.array(
                [e - i for i, e in enumerate(sorted(last_indices))])

        # should be a clojure
        def format_output(idxs):
            if not return_indices:
                return [chunk[i] for i in idxs]
            return (__correct_indices__(idxs, shifts), [chunk[i] for i in idxs])

        assert len(weights) % batch_size == 0
        # optimise the chunk
        index_list = rs.optimise_batch(batch_size, weights)

        # and then yield the chunk batches
        for indices in index_list:
            yield format_output(indices)

        if last_batch:
            yield (tuple(last_indices), last_batch) if return_indices else last_batch

        # when the chunk is empty, load a new one
        chunk = list(itertools.islice(records, chunk_size))
    return StopIteration
