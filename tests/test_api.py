import os
# import pytest


import numpy as np
# import pandas as pd
from morceaux import morceau


os.environ["RUST_BACKTRACE"] = "1"


text = "je suis une liste de mots, tous plus ou moins longs. ici mettre un vrai texte. Ou pas."


def test_basic() -> None:
    L = text.split()
    S = [set(m) for m in morceau(L, 4, size_calc_fn=len)]
    # check we have all the items (if not some in double)
    assert set.union(*S) == set(L)

    # see if we can do the same with other batch sizes
    assert set.union(*[set(m) for m in morceau(L, 3, size_calc_fn=len)]) == set(L)
    assert set.union(*[set(m) for m in morceau(L, 5, size_calc_fn=len)]) == set(L)
    assert set.union(*[set(m) for m in morceau(L, 2, size_calc_fn=len)]) == set(L)


def test_indices() -> None:
    L = text.split()
    indices = []
    new_L = []
    for i, m in morceau(L, 4, return_indices=True, size_calc_fn=len):
        indices += i
        new_L += m
    
    # equivalent to defining new_L as
    # [L[i] for i in indices]
    new_L = [new_L[i] for i in np.argsort(np.array(indices))]
    assert new_L == L

def test_long_input() -> None:
    L = text.split()
    L *= 100

    for _ in morceau(L, 6, size_calc_fn=len):
        pass

    assert True

if __name__ == "__main__":
    test_basic()
    test_indices()
    test_long_input()