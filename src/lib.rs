use std::thread;

use pyo3::prelude::*;
use numpy::{IntoPyArray, PyReadonlyArray1, PyArray};
use ndarray::{Array2, ArrayView1, Axis, Array, Array1};
use rand::Rng;

fn score(matrice: ArrayView1<usize>) -> f32 {
    let sums = matrice.mapv(|x| x as f32);

    let mean = sums.mean().unwrap();

    sums.iter().fold(0., |acc, x| acc + (x - mean)*(x - mean))
}

pub fn optimise(mut matrice: Array2<usize>) -> (Array2<usize>, f32) {
    let mut rng = rand::thread_rng();

    let (k, n) = matrice.dim();
    let mut indices = Array::from_iter(0..(k*n)).into_shape((k, n)).unwrap();
    let mut sums = matrice.sum_axis(Axis(1));
    let mut last = 0;

    let mut s = score(sums.view());
    let mut min = s;
    let mut argmin = indices.clone();
    let mut t = 1./2.;

    // for iter in 0..1_000 {
    let mut iter = 0;
    // println!("{:?}", sums);

    while t > 2e-3 {
        iter += 1;
        // if iter%16 == 0 {
            // let s = score(sums.view());
            // print!("score à itération {iter} : {:.0}_",  s / 1000.);
            // println!("{:.1}", s%1000.);
        // }

        last = 0;
        let cumsum  = Array1::from_iter(sums.iter().map(move |x| {
            last += x;
            last as f64
        }));
        let mut last_inv = 0.;
        let inv_sum = Array1::from_iter(sums.iter().map(move |&x| {
            last_inv +=  1. / (x as f64);
            last_inv as f64
        }));
        // select a random line
        // probas are proportinal to line sum
        let mut line_i = 0;
        let mut line_j = 0;
        let mut r1 = rng.gen::<f64>();
        r1 *= cumsum.last().unwrap();
        while cumsum[line_i] < r1 { line_i += 1; };


        let r2 = rng.gen::<usize>()%k;
        line_j = r2;
        // let mut r2 = rng.gen::<f64>();
        // r2 *= inv_sum.last().unwrap();
        // while inv_sum[line_j] < r2 { line_j += 1; };


        // handle j == i case
        if line_i == line_j { continue; }

        // swap some elements
        let x = rng.gen::<usize>()%n;
        let y = rng.gen::<usize>()%n;

        let tmp = matrice[[line_i, x]];
        // let diff = tmp + matrice[[line_j, y]];
        sums[line_i] = sums[line_i] - tmp + matrice[[line_j, y]];
        sums[line_j] = sums[line_j] - matrice[[line_j, y]] + tmp;

        let ss = score(sums.view());
        if ss < s || rng.gen::<f64>() < t {
            matrice[[line_i, x]] = matrice[[line_j, y]];
            matrice[[line_j, y]] = tmp;
    
    
            let tmp = indices[[line_i, x]];
            indices[[line_i, x]] = indices[[line_j, y]];
            indices[[line_j, y]] = tmp;
            s = ss;
            t *= 0.991;
        } else {
            sums[line_i] = sums[line_i] + tmp - matrice[[line_j, y]];
            sums[line_j] = sums[line_j] + matrice[[line_j, y]] - tmp;
        }

        if s < min {
            min = s;
            argmin = indices.clone();
        }
    }
    // println!("{:.0}", min);
    // println!("{iter}");
    (argmin, min)
}


#[pyfunction]
fn optimise_batch<'py>(
    py: Python<'py>,
    size: usize, weights: PyReadonlyArray1<usize>
) -> &'py PyArray<usize, ndarray::Dim<[usize; 2]>>{


    let weights = weights.as_array().to_owned();
    let n = weights.len();
    let matrice = weights.into_shape((n / size, size)).unwrap();

    // println!("helllo world");
    // let pool = Vec::from_iter(iter);


    let pool = Vec::from_iter((0..6).map(|_| {
        let matrice = matrice.clone();
        thread::spawn(move || {
            // thread::sleep(std::time::Duration::from_millis(1_000));
            // println!("thread started !");
            optimise(matrice)
    })
    }));
    

    let result = pool
        .into_iter()
        .map(|x| x.join().unwrap())
        .reduce(|min, e| if min.1 > e.1 { e } else { min })
        .unwrap();

    let indices = result.0;

    &indices.to_owned().into_pyarray(py)
}


/// A Python module implemented in Rust.
#[pymodule]
#[pyo3(name = "rust_backend")]
fn morceau(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(optimise_batch, m)?)?;
    Ok(())
}
