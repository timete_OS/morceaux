
Morceaux
=================


Ever found yourself iterating in batches, and then everything crashes because of **one** batch ?

**Morceaux** makes sure your batches are constructed in an optimal way, with larger elements packed with the smallest ones.


```python
import numpy as np
import seaborn as sns

from morceaux import morceau

values = np.random.randint(1, 100, 500_000)

batch_size = 16

batch_weights = np.array([sum(v) for v in group_by(values, batch_size)])
better_batch_weights = np.array([sum(v) for v in morceau(values, batch_size)])

print(f"Normal std : {batch_weights.std():.1f}  vs optimised std : {better_batch_weights.std():.1f}")
>>>Normal std : 114.3  vs optimised std : 15.0
```


## TODO

- make it work with PyTorch datasets